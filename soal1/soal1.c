#include <stdio.h>          // standard library for c
#include <stdlib.h>         // standard library for c
#include <sys/types.h>      // process result,id,etc
#include <dirent.h>         // directory listing
#include <sys/stat.h>       // file permission and file ownership
#include <pwd.h>            // pwd.h and grp.h are libraries for file ownership
#include <grp.h>            // along with sys/stat.h
#include <string.h>         // string manipulation
#include <unistd.h>         // fork, exec, etc
#include <time.h>           // Timing control
#include <signal.h>         // Signal controling (for terminating process)
#include <json-c/json.h>    // Working with json files
#include <stdbool.h>        // Boolean variables
#include <fcntl.h>			// Daemon

void initialize(){
	char links[2][100] = {
		"https://drive.google.com/u/0/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp&export=download", // characters
        "https://drive.google.com/u/0/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download" // weapons
	};
	char output_names[2][100] = {
		"characters.zip", "weapons.zip"
	};

	pid_t pid;

	// download files first
	for(int i=0;i<2;i++){
		pid = fork();
		if(pid == 0){
			char *argv[] = {"wget","-q","--no-check-certificate",links[i],"-O",output_names[i],NULL};
            execv("/usr/local/bin/wget",argv);
		}
		else{
			waitpid(pid,NULL,0);
		}
	}

	// extract files
	for(int i=0;i<2;i++){
		pid = fork();
		if(pid == 0){
			char *argv[] = {"unzip","-qq",output_names[i],NULL};
            execv("/usr/bin/unzip",argv);
		}
		else{
			waitpid(pid,NULL,0);
		}
	}

	// make a new directory
	pid = fork();
	if(pid == 0){
		char *argv[] = {"mkdir", "-p","gacha_gacha",NULL};
        execv("/bin/mkdir", argv);
	}
	waitpid(pid,NULL,0);
    
}
void make_folder(int n, char name[150]){
	char num[10];
	char folder_name[150] = "total_gacha_";
	sprintf(num,"%d",n); strcat(folder_name,num); strcpy(name,folder_name);

	pid_t pid;
	pid = fork();
	if(pid == 0){
		char *argv[] = {"mkdir","-p",folder_name,NULL};
		execv("/bin/mkdir", argv);
	}
	else{
		waitpid(pid,NULL,0);
	}
}
void move_txt(char txt_arr[][150], char folder_name[150]){
	pid_t pid;
	for(int i=0;i<9;i++){
		//printf("%s %s\n", txt_arr[i], folder_name);
		pid = fork();
		if(pid == 0){
			char *argv[] = {"mv",txt_arr[i], folder_name, NULL};
			execv("/bin/mv", argv);
		}
		else{
			waitpid(pid,NULL,0);
		}
	}
}
void make_txt(int n, int hour, int minute, int second, char txt[150]){
	char hr[100]; char min[100]; char sec[100]; char num[10];
	sprintf(hr,"%d", hour); sprintf(min, "%d", minute); sprintf(sec, "%d", second); sprintf(num,"%d", n);
	if(hour >= 0 && hour <= 9){
		char zero[100] = "0";
		strcat(zero,hr);
		strcpy(hr, zero);
	}
	if(minute >= 0 && minute <= 9){
		char zero[100] = "0";
		strcat(zero,min);
		strcpy(min, zero);
	}
	if(second >= 0 && second <= 9){
		char zero[100] = "0";
		strcat(zero,sec);
		strcpy(sec, zero);
	}
	
	strcat(hr,":");strcat(min,":");strcat(hr,min);strcat(hr,sec);strcat(hr,"_gacha_"); strcat(hr,num);
	strcat(hr,".txt");
	strcpy(txt,hr);
}
void put_txt(char str_arr[][150], char txt_name[150]){
	FILE *fp = fopen(txt_name,"w+");
	for(int i=0; i<10; i++){
		fputs(str_arr[i],fp);
		fprintf(fp,"\n");
	}
	fclose(fp);
}

void gacha(int n, char container[150], int primogems){
	// change directory to home
	char wd[] = "/Users/rainataputra/Documents/4th Term/Operating System/soal1_shift2";
	chdir(wd);
	char type[100];
	int num;
	if(n % 2 == 0){
		int lower = 1;
		int upper = 130;
		num =  (rand() % (upper - lower + 1)) + lower;
		strcpy(type, "weapons");
		
	}
	else{
		strcpy(type, "characters");
		int lower = 1;
        int upper = 48;
        num =  (rand() % (upper - lower + 1)) + lower;
	}
	//printf("penentuan tipe\n");

	// read dir and grab a random json file
	DIR *dp;
	struct dirent *en;
	dp = opendir(type);
	int i=0;
	char json_name[100];
	if(dp){
		while((en = readdir(dp)) != NULL && i < num){
			if(strcmp(en->d_name,".") != 0 && strcmp(en->d_name,"..") != 0){
				if(i == num-1){
					strcpy(json_name, en->d_name);
					closedir(dp);
				}
				++i;
			}
		}
	}
	//printf("reading file %s\n", json_name);

	// Working with JSON file
	chdir(type);
	FILE *fp;
    char buffer[4096];

    struct json_object *parsed_json;
    struct json_object *name;
    struct json_object *rarity;

    fp = fopen(json_name, "r");
    fread(buffer, 4096, 1, fp);
    fclose(fp);

    parsed_json = json_tokener_parse(buffer);
    json_object_object_get_ex(parsed_json,"name", &name);
    json_object_object_get_ex(parsed_json,"rarity", &rarity);

	char rarity_name[150];
	char number[150];
	char gems[10];
	sprintf(number,"%d",n); sprintf(gems,"%d", primogems);
    strcpy(rarity_name,number); strcat(rarity_name,"_"); strcat(rarity_name,type); 
	strcat(rarity_name,"_"); strcat(rarity_name, json_object_get_string(rarity));
    strcat(rarity_name, "_"); strcat(rarity_name, json_object_get_string(name)); 
	strcat(rarity_name,"_"); strcat(rarity_name,gems);
	

	strcpy(container,rarity_name);
	chdir(wd);
}

void zip_dir(char dir[150]){
	pid_t pid;
	pid = fork();
	char zip_name[150] = "not_safe_for_wibu.zip";
	char password[150] = "satuduatiga";
	

	if(pid == 0){
		char *argv[] = {"zip","-P",password, "-r", zip_name, dir, NULL};
		execv("/usr/bin/zip", argv);
	}
	else{
		waitpid(pid,NULL,0);
	}
}

int main() {
  pid_t pid, sid;        // Variabel untuk menyimpan PID

  pid = fork();     // Menyimpan PID dari Child Process

  /* Keluar saat fork gagal
  * (nilai variabel pid < 0) */
  if (pid < 0) {
    exit(EXIT_FAILURE);
  }

  /* Keluar saat fork berhasil
  * (nilai variabel pid adalah PID dari child process) */
  if (pid > 0) {
    exit(EXIT_SUCCESS);
  }

  umask(0);

  sid = setsid();
  if (sid < 0) {
    exit(EXIT_FAILURE);
  }

  if ((chdir("/Users/rainataputra/Documents/4th Term/Operating System/soal1_shift2")) < 0) {
    exit(EXIT_FAILURE);
  }

  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);

	bool step1 = false;

  while (1) {
	// Grab the the time
    time_t rawtime;
    struct tm * timeinfo;
    time(&rawtime);
    timeinfo = localtime(&rawtime);

    // Grab day, month, hour and min
    int day = timeinfo->tm_mday;
    int month = timeinfo->tm_mon + 1;
    int hour = timeinfo->tm_hour;
	int del_time = hour + 3;
    int minute = timeinfo->tm_min;
	int second = timeinfo->tm_sec;

	if(day >= 30 && month >= 3 && hour >= 4 && minute >= 44 && !step1){
		step1 = true;
		char chara_weapons_container[10][150];
		char txt_name[9][150];
		int index = 0;
		int txt_index = 0;
		int primogems = 79000;
		int times = primogems/150;

		initialize();

		for(int i=1;i<=times;i++){
			primogems -= 160;
			gacha(i,chara_weapons_container[index],primogems);
			++index;
			chdir("gacha_gacha");
			if(i % 10 == 0){
				index = 0;
				char txt[150];
				make_txt(i, hour, minute, second,txt);

				// time adjustment
				if(++second >= 60){
					second %= 60;
					++minute;
				}
				if(minute >= 60){
					minute %= 60;
					++hour;
				}
				if(hour >= 24){
					hour %= 24;
				}

				strcpy(txt_name[txt_index++], txt);
				put_txt(chara_weapons_container, txt);
			}
			if(i % 90 == 0){
				txt_index = 0;
				char folder_name[150];
				make_folder(i,folder_name);
				move_txt(txt_name,folder_name);
			}
		}

	}
	hour = timeinfo->tm_hour;

	if(hour >= del_time && step1){
		chdir("/Users/rainataputra/Documents/4th Term/Operating System/soal1_shift2");
		zip_dir("gacha_gacha");
		char *argv[] = {"rm","-r","-d","gacha_gacha", "characters", "weapons", "characters.zip", "weapons.zip", NULL};
		execv("/bin/rm", argv);
	}

    sleep(10);
  }
}
