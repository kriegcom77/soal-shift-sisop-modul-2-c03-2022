#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <sys/stat.h>
#include <dirent.h>
#include <stdio.h>
#include <string.h>


int main() {
  pid_t child_a, child_b, child_c, child_d;
  int status;
  child_a = fork();
  if (child_a < 0) 
  {
    exit(EXIT_FAILURE); 
  }
  //no 3a
  if (child_a == 0) 
  {
    char *mkd[] = {"mkdir", "/home/ozoraa/modul2/darat", NULL};
    execv("/bin/mkdir", mkd);
  } 
  else 
  {
    child_b = fork();
    if (child_b < 0) 
    {
      exit(EXIT_FAILURE); 
    }
    if (child_b == 0)
    { 
      while ((wait(&status)) > 0);
      sleep(3);
      char *argv[] = {"mkdir","/home/ozoraa/modul2/air", NULL};
      execv("/bin/mkdir", argv);
    }
    else 
    {
      while ((wait(&status)) > 0);
      child_c = fork();
      if (child_c < 0) 
      {
        exit(EXIT_FAILURE); 
      }
        //no 3b
      if(child_c == 0) 
      {
        char *unzip[]={"unzip", "/home/ozoraa/modul2/animal.zip", NULL};
        execv("/usr/bin/unzip",unzip);
      }   
      else 
      {
        while ((wait(&status)) > 0);
        DIR *d;
        struct dirent *dir; 
        d = opendir("animal");
        int masuk = 0;
        if (d)
        {
          while ((dir = readdir(d)) != NULL)
          {
            masuk++;
            // printf("masuk sini bang %d\n",masuk);
            char s[10] = {'\0'};
            char dir_name[1000] = {'\0'};
            child_d = fork ();
            if (child_d < 0) 
            {
              exit(EXIT_FAILURE); 
            }
            if (child_d == 0)
            {
              struct stat info;

              char namafile[1000];
              sprintf(namafile, "/home/ozoraa/modul2/animal/%s", dir->d_name);
              strcpy(dir_name,dir->d_name);
              // printf("%s\n",namafile);
              //
              //butuh loop untuk cek nama file setelah di extract
              for(int i = 0; i < strlen(dir_name); i++)
              {
                if(dir_name[i]=='_'&&dir_name[i+1]=='d' || dir_name[i]=='_'&&dir_name[i+1]=='b')

                {
                  strcpy(s,"darat");
                }
                else if( (dir_name[i]=='_'&&(dir_name[i+1]=='a'||dir_name[i+1]=='i')))
                {
                  strcpy(s,"air");
        //      printf("%s\n",dir_name);
                }
              }

              if (stat(namafile, &info)) 
              {
                exit (EXIT_FAILURE);
              }
              
              if(strcmp(s,"darat") == 0)
              {
                char* move[] = {"mv", namafile, "/home/ozoraa/modul2/darat/", NULL}; 
                execv("/bin/mv", move);

              }
              if(strcmp(s,"air") == 0)
              {
                 char* move[] = {"mv", namafile, "/home/ozoraa/modul2/air/", NULL};
                 execv("/bin/mv", move);             
              }
              else{
                 // char* rmve[] = {"rm", namafile, "/home/ozoraa/modul2/air/", NULL};
                 // execv("/bin/rm", rmve); 
                remove(namafile);
              }
            }
          }
          closedir(d);
        }
      }

              //nomor 3 D

                DIR *d;
                struct dirent *dir; 
                d = opendir("/home/ozoraa/modul2/darat");

                if(d)
                {
                  //printf("masik di hapus file\n");
                  while ((dir = readdir(d)) != NULL)
                  {
                    char s[10] = {'\0'};  
                    char dir_name[1000] = {'\0'};

                    char namafile[1000];
                    sprintf(namafile, "/home/ozoraa/modul2/darat/%s", dir->d_name);
                    strcpy(dir_name,dir->d_name);
                    // printf("%s\n",namafile);
                    for(int i = 0; i < strlen(namafile); i++)
                    {
                      if(namafile[i]=='b'&&namafile[i+1]=='i'&&namafile[i+2]=='r'&&namafile[i+3]=='d')
                      {
                        strcpy(s,"bird");

                       // printf("%s\n",namafile);
                      }
                    }
                    if(strcmp(s,"bird")==0)
                    {
                      remove(namafile);
                    }
                  }
                 
                }
 closedir(d);
            
               }
DIR *d;
               struct dirent *dir; 
               d = opendir("/home/ozoraa/modul2/air");
               struct stat fileStat;
               char full_path[1000];
               char path[1000]="/home/ozoraa/modul2/air";
                char *filename = "/home/ozoraa/modul2/air/list.txt";
                FILE *fp = fopen(filename, "w");
                if(d)

                {
                  
                  while ((dir = readdir(d)) != NULL)
                  {
                    

                      if(dir->d_type==DT_REG)
                      {
                      full_path[0]='\0';
                  
                        printf ("%s ", dir->d_name);
                        strcpy (full_path, path);
                        strcat (full_path, "/");
                        strcat (full_path, dir->d_name);

                        if (!stat(path, &fileStat))
                        {

                         fprintf(fp,"conan_");
                           fprintf(fp,(S_ISDIR(fileStat.st_mode))  ? "d" : "-");
                           fprintf(fp,(fileStat.st_mode & S_IRUSR) ? "r" : "-");
                           fprintf(fp,(fileStat.st_mode & S_IWUSR) ? "w" : "-");
                           fprintf(fp,(fileStat.st_mode & S_IXUSR) ? "x" : "-");
                           fprintf(fp,(fileStat.st_mode & S_IRGRP) ? "r" : "-");
                           fprintf(fp,(fileStat.st_mode & S_IWGRP) ? "w" : "-");
                           fprintf(fp,(fileStat.st_mode & S_IXGRP) ? "x" : "-");
                           fprintf(fp,(fileStat.st_mode & S_IROTH) ? "r" : "-");
                           fprintf(fp,(fileStat.st_mode & S_IWOTH) ? "w" : "-");
                           fprintf(fp,(fileStat.st_mode & S_IXOTH) ? "x" : "-");
                        } else
                        {
                            perror("Error in stat");
                        }
                        fprintf( fp,"_%s\n",dir->d_name);
                        
                         }
       
                      }
            fclose(fp);
            closedir(d);
                    }


     }
 }