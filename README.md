# soal-shift-sisop-modul-1-C03-2022



## Anggota Kelompok



```
Nama    : Ida Bagus Kade Rainata Putra Wibawa
Nrp     : 5025201235

Nama    : Moh. Ilham Fakhri Zamzami
Nrp     : 5025201275

Nama    : Ichsanul Aulia
Nrp     : 05111840007001
```
<div style="text-align: justify">

## Nomer 1
**Penjelasan**: Pada dasarnya soal 1 meminta kami membuat mekanisme gacha untuk user Refaldi dari characters dan weapons Genshin impact.
File characters dan weapons harus didownload melalui program yang ditulis serta extract pada program juga. Untuk keterangan lebih lanjut bisa dilihat pada poin 1A, 1B, 1C, 1D, dan 1E.

### KENDALA
1. Keterbatasan materi pada modul sehingga harus mencari tambahan materi di internet.
2. Sesuatu yang cukup baru.

### 1A
**Penjelasan**: Poin 1A meminta kami untuk mendownload 2 files lalu mengextractnya. Selanjutnya, kami diminta untuk membuat folder yangf diberi nama **gacha_gacha** yang akan dijadikan working directory.

**Penyelesaian**: Hal yang kami lakukan adalah membuat 2 array of strings untuk menyimpan links files dan juga nama output dari files yang akan didownload. Hal ini akan memudahkan proses extract nantinya. Untuk perilah download, kami menggunakan **for loop dengan range 2** karena files yang didownload 2 files dan pada setiap iterasi, kami memanggil **fork()** yang child prosesnya kami gunakan untuk mendownload file. Pada parent proses hanya perlu menunggu proses download selesai dengan **waitpid()**. Hal yang sama juga dilakukan untuk proses extract. Berikutnya, hanya perlu membuat folder **gacha_gacha** dan menjadikannya current working directory dengan fungsi **chdir()**. Proses ini kami selesaikan dengan membuat fungsi yang bernama **initialize()**

``` c
. . . .
void initialize(){
	char links[2][100] = {
		"https://drive.google.com/u/0/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp&export=download", // characters
        "https://drive.google.com/u/0/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download" // weapons
	};
	char output_names[2][100] = {
		"characters.zip", "weapons.zip"
	};

	pid_t pid;

	// download files first
	for(int i=0;i<2;i++){
		pid = fork();
		if(pid == 0){
			char *argv[] = {"wget","-q","--no-check-certificate",links[i],"-O",output_names[i],NULL};
            execv("/usr/local/bin/wget",argv);
		}
		else{
			waitpid(pid,NULL,0);
		}
	}

	// extract files
	for(int i=0;i<2;i++){
		pid = fork();
		if(pid == 0){
			char *argv[] = {"unzip","-qq",output_names[i],NULL};
            execv("/usr/bin/unzip",argv);
		}
		else{
			waitpid(pid,NULL,0);
		}
	}

	// make a new directory
	pid = fork();
	if(pid == 0){
		char *argv[] = {"mkdir", "-p","gacha_gacha",NULL};
        execv("/bin/mkdir", argv);
	}
	waitpid(pid,NULL,0);
    
}
. . . .
```

### 1B
**Penjelasan**: Poin 1B meminta kami untuk membuat sistem gacha agar character dan weapon tergacha secara bergantian, dengan gacha ganjil berupa character dan genap berupa weapon. Ketika jumlah gachanya dapat dimodulo 10, maka hasil-hasil gacha tersebut akan dimasukkan ke dalam sebuah .txt file. Jika giliran gacha dapat dimodulo 90, maka dibuatlah suatu folder yang di dalamnya berisi file-file .txt yang sudah dibuat tadi. Hal ini mengakibatkan didalam setiap file .txt terdapat 10 hasil gacha dan setiap folder terdapat 9 file .txt.

**Penyelesaian**: Kami menyelesaikan soal ini dengan membuat **array or strings berkapasitas 10** untuk menyimpan hasil gacha dan **array of strings berukuran 9 untuk menyimpan nama file .txt** yang dibuat. Proses pemindahan tentunya terjadi sesuai dengan aturan pada soal, jika modulo 10 maka pindahkan hasil gacha ke .txt file. Jika modulo 9, pindahkan files .txt ke folder. Semua ini terjadi pada **for loop** yang dimulai dari 1 dan berakhir pada kuota maksimal gacha.

``` c
. . . .
char chara_weapons_container[10][150];
		char txt_name[9][150];
		int index = 0;
		int txt_index = 0;
. . . .

for(int i=1;i<=times;i++){
			primogems -= 160;
			gacha(i,chara_weapons_container[index],primogems);
			++index;
			chdir("gacha_gacha");
			if(i % 10 == 0){
				index = 0;
				char txt[150];
				make_txt(i, hour, minute, second,txt);

				// time adjustment
				if(++second >= 60){
					second %= 60;
					++minute;
				}
				if(minute >= 60){
					minute %= 60;
					++hour;
				}
				if(hour >= 24){
					hour %= 24;
				}

				strcpy(txt_name[txt_index++], txt);
				put_txt(chara_weapons_container, txt);
			}
			if(i % 90 == 0){
				txt_index = 0;
				char folder_name[150];
				make_folder(i,folder_name);
				move_txt(txt_name,folder_name);
			}
		}
. . . .
```

### 1C dan 1D
**Penjelasan**: Format penamaan file haruslah **{Hh:Mm:Ss}_gacha_{jumlah-gacha}**, misal **04:44:12_gacha_120.txt**, dan format penamaan untuk setiap folder nya adalah **total_gacha_{jumlah-gacha}**, misal **total_gacha_270**. Setiap file .txt akan memiliki perbedaan penamaan output sebesar 1 second.

**Penyelesaian**:

1. **Mekanisme Gacha**

Mekanisme gacha dilakukan pada fungsi yang kami beri nama **gacha()** yang memiliki parameter **n (turn gacha)**, **container (untuk menyimpan hasil gacha)**, **primogems (jumlah primogems)**. Pertama, cek apakah giliran gacha genap atau ganjil. Jika ganjil, set **variable type menjadi characters** dan sebaliknya **set menjadi weapon apabila genap**. Jangan lupa untuk set nilai random (untuk proses gacha) sesuai dengan range jumlah file yang tersedia pada folder characters dan weapons. Untuk weapons sendiri di set **lower = 1 ** dan **upper = 130** karena jumlah filenya 131. Untuk characters, **lower = 1** dan **upper = 48** karena jumlah filenya 49. Setelah itu dilakukan proses **reading directory** sesuai dengan type (character atau weapon), lalu dilakukan iterasi sebanyak random number yang dimiliki **agar sesuai mengikuti asas gacha yang random**. Setelah file gacha sudah didapatkan, dilanjutkan dengan proses **parsing json file** karena file yang diambil masih berformat JSON. proses ini dilakukan dengan bantuan library **json.h**. Proses parsing dilakukan untuk mengambil atribut **nama dan rarity item**. Setelah nama dan rarity didapatkan, hanya perlu menyatukan komponen rarity dan nama agar sesuai dengan format nama hasil gacha yang diminta. Proses ini melibatkan fungsi-fungsi pada library **string.h**.

```c
void gacha(int n, char container[150], int primogems){
	// change directory to home
	char wd[] = "/Users/rainataputra/Documents/4th Term/Operating System/soal1_shift2";
	chdir(wd);
	char type[100];
	int num;
	if(n % 2 == 0){
		int lower = 1;
		int upper = 130;
		num =  (rand() % (upper - lower + 1)) + lower;
		strcpy(type, "weapons");
		
	}
	else{
		strcpy(type, "characters");
		int lower = 1;
        int upper = 48;
        num =  (rand() % (upper - lower + 1)) + lower;
	}
	//printf("penentuan tipe\n");

	// read dir and grab a random json file
	DIR *dp;
	struct dirent *en;
	dp = opendir(type);
	int i=0;
	char json_name[100];
	if(dp){
		while((en = readdir(dp)) != NULL && i < num){
			if(strcmp(en->d_name,".") != 0 && strcmp(en->d_name,"..") != 0){
				if(i == num-1){
					strcpy(json_name, en->d_name);
					closedir(dp);
				}
				++i;
			}
		}
	}
	//printf("reading file %s\n", json_name);

	// Working with JSON file
	chdir(type);
	FILE *fp;
    char buffer[4096];

    struct json_object *parsed_json;
    struct json_object *name;
    struct json_object *rarity;

    fp = fopen(json_name, "r");
    fread(buffer, 4096, 1, fp);
    fclose(fp);

    parsed_json = json_tokener_parse(buffer);
    json_object_object_get_ex(parsed_json,"name", &name);
    json_object_object_get_ex(parsed_json,"rarity", &rarity);

	char rarity_name[150];
	char number[150];
	char gems[10];
	sprintf(number,"%d",n); sprintf(gems,"%d", primogems);
    strcpy(rarity_name,number); strcat(rarity_name,"_"); strcat(rarity_name,type); 
	strcat(rarity_name,"_"); strcat(rarity_name, json_object_get_string(rarity));
    strcat(rarity_name, "_"); strcat(rarity_name, json_object_get_string(name)); 
	strcat(rarity_name,"_"); strcat(rarity_name,gems);
	

	strcpy(container,rarity_name);
	chdir(wd);
}
```

2. **Membuat .txt file**

Untuk permasalah ini, kami membuat fungsi yang bernama **make_txt()** dengan  parameter **n (giliran gacha), hour, minute, second, dan txt (string untuk menyimpan nama .txt file)**. Pertama, lakukan perubahan pada waktu yang semula bertipe int menjadi string untuk proses concatenate string nantinya (lakukan dengan **sprintf**). Selanjutnya, lakukan adjustment pada waktu yang berjumlah **1 digit** yaitu dengan menambahkan character **0** dibagian depannya. Setelah itu hanya perlu menggabungkan strings sesuai dengan nama yang diminta.

```c
void make_txt(int n, int hour, int minute, int second, char txt[150]){
	char hr[100]; char min[100]; char sec[100]; char num[10];
	sprintf(hr,"%d", hour); sprintf(min, "%d", minute); sprintf(sec, "%d", second); sprintf(num,"%d", n);
	if(hour >= 0 && hour <= 9){
		char zero[100] = "0";
		strcat(zero,hr);
		strcpy(hr, zero);
	}
	if(minute >= 0 && minute <= 9){
		char zero[100] = "0";
		strcat(zero,min);
		strcpy(min, zero);
	}
	if(second >= 0 && second <= 9){
		char zero[100] = "0";
		strcat(zero,sec);
		strcpy(sec, zero);
	}
	
	strcat(hr,":");strcat(min,":");strcat(hr,min);strcat(hr,sec);strcat(hr,"_gacha_"); strcat(hr,num);
	strcat(hr,".txt");
	strcpy(txt,hr);
}
```

3. **Menaruh hasil gacha ke .txt file**

Proses ini dilakukan dengan fungsi yang kami beri nama **put_txt()**. Hal ini cukup sederhana, hanya perlu memindahkan seluruh hasil gacha pada array of strings hasil gacha ke file .txt yang sudah dibuat.

```c
void put_txt(char str_arr[][150], char txt_name[150]){
	FILE *fp = fopen(txt_name,"w+");
	for(int i=0; i<10; i++){
		fputs(str_arr[i],fp);
		fprintf(fp,"\n");
	}
	fclose(fp);
}
```

4. **Mebuat folder ketika modulo 90**

Proses ini kami selesaikan dengan fungsi **make_folder()**. Cukup sederhana, hanya perlu menggunakan **fork()** dan **mkdir** untuk membuat folder.

```c
void make_folder(int n, char name[150]){
	char num[10];
	char folder_name[150] = "total_gacha_";
	sprintf(num,"%d",n); strcat(folder_name,num); strcpy(name,folder_name);

	pid_t pid;
	pid = fork();
	if(pid == 0){
		char *argv[] = {"mkdir","-p",folder_name,NULL};
		execv("/bin/mkdir", argv);
	}
	else{
		waitpid(pid,NULL,0);
	}
}
```

5. **Menaruh file .txt ke dalam folder**
Permasalahan ini kami selesaikan dengan fungsi **move_txt()** dengan parameter array of strings .txt (nama txt filenya) dan nama foldernya. Selanjutnya digunakan **for loop, fork()**, juga command **mv** untuk memindahkan txt file ke folder.

```c
void move_txt(char txt_arr[][150], char folder_name[150]){
	pid_t pid;
	for(int i=0;i<9;i++){
		//printf("%s %s\n", txt_arr[i], folder_name);
		pid = fork();
		if(pid == 0){
			char *argv[] = {"mv",txt_arr[i], folder_name, NULL};
			execv("/bin/mv", argv);
		}
		else{
			waitpid(pid,NULL,0);
		}
	}
}
```

6. **Main For loop untuk proses Gacha**

Pada tahap ini dibuat for loop sesuai dengan jumlah kuota gacha (primogems/160). Selanjutnya dilakukan adjustment primogems dan penyesuaian waktu untuk penamaan file .txt.

```c
. . . .
step1 = true;
		char chara_weapons_container[10][150];
		char txt_name[9][150];
		int index = 0;
		int txt_index = 0;
		int primogems = 79000;
		int times = primogems/150;

		initialize();

		for(int i=1;i<=times;i++){
			primogems -= 160;
			gacha(i,chara_weapons_container[index],primogems);
			++index;
			chdir("gacha_gacha");
			if(i % 10 == 0){
				index = 0;
				char txt[150];
				make_txt(i, hour, minute, second,txt);

				// time adjustment
				if(++second >= 60){
					second %= 60;
					++minute;
				}
				if(minute >= 60){
					minute %= 60;
					++hour;
				}
				if(hour >= 24){
					hour %= 24;
				}

				strcpy(txt_name[txt_index++], txt);
				put_txt(chara_weapons_container, txt);
			}
			if(i % 90 == 0){
				txt_index = 0;
				char folder_name[150];
				make_folder(i,folder_name);
				move_txt(txt_name,folder_name);
			}
		}
. . . .
```

### 1E
**Penjelasan**: Proses gacha tadi akan dilakukan pada tanggal **30 Maret jam 04.44** 2022 dan **3 jam** setelahnya semua isi pada folder **gacha_gacha** akan di zip dengan nama **not_safe_for_wibu** dan diberi password **satuduatiga** lalu **menghapus semua file** dan hanya menyisakan file .zip.

**Penyelesaian**:

1. **Melakukan gacha pada tanggal yang diminta**

Hal ini dapat diselesaikan dengan memuat seluruh kode tadi pada daemon. Hal yang perlu dilakukan adalah cukup mengambil waktu dengan bantuan library **time.h** dan mengambil atribut **day, month, hour, minute** (jangan lupa untuk menambah satu pada atribut bulan karena bulan yang diberikan -1 dari bulan yang sebenarnya). Lalu tinggal membuat **conditional if** pada **while daemon** sesuai dengan tanggal yang diminta.

 ```c
 . . . .
while (1) {
	// Grab the the time
    time_t rawtime;
    struct tm * timeinfo;
    time(&rawtime);
    timeinfo = localtime(&rawtime);

    // Grab day, month, hour and min
    int day = timeinfo->tm_mday;
    int month = timeinfo->tm_mon + 1;
    int hour = timeinfo->tm_hour;
	int del_time = hour + 3;
    int minute = timeinfo->tm_min;
	int second = timeinfo->tm_sec;

	if(day >= 30 && month >= 3 && hour >= 4 && minute >= 44 && !step1){
. . . .
```

2. **Proses zip dan delete**

Proses zip dilakukan dengan fungsi yang kami beri nama **zip_dir()** dengan parameter nama directory yang ingin di zip. Selanjutnya hanya perlu menggunakan execv untuk menzipnya (diberi nama dan password yang diminta soal).

```c
. . . .
void zip_dir(char dir[150]){
	pid_t pid;
	pid = fork();
	char zip_name[150] = "not_safe_for_wibu.zip";
	char password[150] = "satuduatiga";
	

	if(pid == 0){
		char *argv[] = {"zip","-P",password, "-r", zip_name, dir, NULL};
		execv("/usr/bin/zip", argv);
	}
	else{
		waitpid(pid,NULL,0);
	}
}
. . . .
```

Proses delete files dilakukan pada bagian akhir fungsi main. Proses penghapusan ini tidak dilakukan dengan fork sehingga akan mematikan daemon secara otomatis (tidak perlu membunuh proses di terminal). **Seluruh proses zip dan delete dilakukan setelah 3 jam proses gacha tadi** sehingga perlu dibuat kondisi sebelum mengeksekusi perintah zip dan delete.

```c
 . . . .
hour = timeinfo->tm_hour;

	if(hour >= del_time && step1){
		chdir("/Users/rainataputra/Documents/4th Term/Operating System/soal1_shift2");
		zip_dir("gacha_gacha");
		char *argv[] = {"rm","-r","-d","gacha_gacha", "characters", "weapons", "characters.zip", "weapons.zip", NULL};
		execv("/bin/rm", argv);
	}
. . . .
```

### HASIL NO 1 KESELURUHAN

![Screen_Shot_2022-03-27_at_17.20.56](/uploads/b73e2cea80a4e00b05d049b08511c386/Screen_Shot_2022-03-27_at_17.20.56.png)

![Screen_Shot_2022-03-27_at_17.21.30](/uploads/9513c183032d221438e343381f8fc92d/Screen_Shot_2022-03-27_at_17.21.30.png)

![Screen_Shot_2022-03-27_at_17.21.57](/uploads/629224f232bfad2de63d743d9dcb0d20/Screen_Shot_2022-03-27_at_17.21.57.png)

![Screen_Shot_2022-03-27_at_17.22.25](/uploads/c18513ca6e71bd442ffc960a92280951/Screen_Shot_2022-03-27_at_17.22.25.png)

## Nomer 2
Japrun bekerja di sebuah perusahaan dibidang review industri perfilman, karena kondisi saat ini sedang pandemi Covid-19, dia mendapatkan sebuah proyek untuk mencari drama korea yang tayang dan sedang ramai di Layanan Streaming Film untuk diberi review. Japrun sudah mendapatkan beberapa foto-foto poster serial dalam bentuk zip untuk diberikan review, tetapi didalam zip tersebut banyak sekali poster drama korea dan dia harus memisahkan poster-poster drama korea tersebut tergantung dengan kategorinya. Japrun merasa kesulitan untuk melakukan pekerjaannya secara manual, kamu sebagai programmer diminta Japrun untuk menyelesaikan pekerjaannya.

inisiasi working space kedalam beberapa variebel untuk mempermudah dalam menulisakan code
```
char *locpwd="/home/ilhamfz/Sisop/Shift2/drakor";
char *loczip="/home/ilhamfz/Sisop/Shift2/drakor.zip";
```
buat struct drakor juga untuk menyimpan setiap data dari poster drakor yang tersedia.
```
struct drakor {
    char file_name[1001];
    char title[101];
    int year;
    char genre[101];
};
```
### 2A
ekstrak file zip kedalam “/home/ilhamfz/shift2/drakor”
```
...
    if (child_a == 0) {
        char *argv[] = {"mkdir", "-p", locpwd, NULL};
        execv("/bin/mkdir", argv);
    } else {
...
```
Diinialisasi pid dan gunakan fork, saat child_a bernilai 0 (child), maka gunakan execv untuk membuat folder dengan nama drakor dengan `mkdir`, selanjutnya unzip dengan menginialisai pid child_ahalf pada parent dari pid child_a. Namun, jangan lupa untuk memberikan `while ((wait(&status)) > 0);` untuk menunggu folder drakor telah terbuat terlebih dahulu. gunakan fork juga pada pid child_ahalf.
```
...
        if (child_ahalf == 0){
            char *argv[] = {"unzip", "-d", locpwd, loczip, NULL};
            execv("/usr/bin/unzip", argv);
        } else{
...
```
Gunakan `unzip` untuk me-ekstrak folder drakor.zip, lalu hapus folder ataupun file yang tidak penting (folder dan file selain .png).
```
...
            if (child_b == 0){
                char *argv[] = {"find", locpwd, "-mindepth", "1", "-maxdepth", "1", "-not", "-name", "*.png", "-exec", "rm", "-rf", "{}", "+", NULL};
                execv("/usr/bin/find", argv);
            } else  {
...
```
untuk menghapusnya, digunakan perpaduan dari `find` syntax dan bantuan dr `exec`, lalu gunakan `rm -rf` file selain format .png yang ada didalam pwd.

### 2B
Buat folder dengan nama genre dari setiap isi dari folder drakor.zip yang tersedia dengan cara :
1. gunakan lib `dirent.h`, baca semua file pada `locpwd` dengan menggunakan 
```
...
            while ((dp = readdir(dir)) != NULL){
                    if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0){
...
```
2. ambil setiap data dari file name, masukkan setiap data kedalam struct drakor yang telah diinialisasi sebelumnya, gunakan juga `strtok` untuk mengambil data title, year, dan genre dari setiap file yang tersedia.
```
...
                        int another_flag=0;
                        strcpy(data[flag].file_name, dp->d_name);
                        if(strchr(data[flag].file_name, '_')!=0) another_flag++;
                        char *tok = strtok(dp->d_name, "_;.");
                        for(int i=0; tok!=NULL; i++){
                            if(i==0) strcpy(data[flag].title, tok);
                            else if (i==1) data[flag].year=atoi(tok);
                            else if (i==2) strcpy(data[flag].genre, tok);
                            else if (i==3) { //iterasi sampai pada '_' atau '.png'
                                flag++;
                                if(another_flag!=0){
                                    strcpy(data[flag].file_name, data[flag-1].file_name);
                                    strcpy(data[flag].title, tok);
                                    another_flag=0;
                                }
                                i=0;
                            }
                            tok = strtok (NULL, "_;.");
                        }
...
```
dicatat jumlah dari poster yang ada/dibutuhkan dalam variabel `flag`, strtok digunakan dengan syarat `_`, `;`, `.`. Adapun digunakan `strchr` untuk mencari nama file yang menunjukkan dua poster sekaligus, tandai file tersebut dan catat data tersebut dengan nama file yang sama pada array of struct setelahnya.
3. setelah didapat semua data pada lockpwd, kirim data struct dan iterasi(index) looping sebanyak jumlah flag untuk membuat folder dengan melengkapi path sesuai genre apa saja yang tersedia dalam setiap nama file di `lockpwd`
```
    char temp[101];
    strcpy(temp, path);
    strcat(temp, "/");
    strcat(temp, (ptr+index)->genre);
    ...
    if (func == 0){
        char *argv[] = {"mkdir", "-p", temp, NULL};
        execv("/bin/mkdir", argv);
    } else {
        while ((wait(&status)) > 0);
        return;
```
buat folder dengan syntax `mkdir -p` dan jangan lupa untuk gunakan pid dan fork dan wait juga pada fungsi tersebut.
### 2C
pindah setiap poster kedalam masing-masing genre folder yang sudah dibuat sebelumnya.
```
...
                for(int i=0; i<flag; i++) copy_movre(data, i, locpwd);
...
char from[101];
    strcpy(from, path);
    strcat(from, "/");
    strcat(from, (ptr+index)->file_name);
    char dest[101];
    strcpy(dest, path);
    strcat(dest, "/");
    strcat(dest, (ptr+index)->genre);
    strcat(dest, "/");
    strcat(dest, (ptr+index)->title);
    strcat(dest, ".png");
...
```
seperti sebelumnya, kirim data struct dan index loop sebanyak flag kedalam fungsi. Dalam fungsi tersebut, buat path untuk mencatat asal file dengan memasukkan nama filenya dan path untuk tujuan file dengan memasukkan genre file sebagai foldernya dan ubah nama file dengan data title dari masing-masing file tersebut.
```
    if (func == 0){
        char *argv[] = {"cp", from, dest, NULL};
        execv("/usr/bin/cp", argv);
    }else {
        while ((wait(&status)) > 0);
        return;
```
digunakan syntax `cp` untuk me-copy file poster dari path `from` dan di paste kedalam path `dest`.
### 2D
ada kasus dimana satu file terdiri dari dua poster dimana nama file tersebut dipisahkan oleh `_`. masukkan file tersebut kedalam dua folder dengan genre masing-masing posternya.
```
...
                        int another_flag=0;
                        ...
                        if(strchr(data[flag].file_name, '_')!=0) another_flag++;
                        ...
                                if(another_flag!=0){
                                    strcpy(data[flag].file_name, data[flag-1].file_name);
                                    strcpy(data[flag].title, tok);
                                    another_flag=0;
                                }
...
```
seperti beberapa step yang telah dijelaskan pada point 2b, gunakan `strchr` jika file tersebut merupakan kasus khusus, berikan tanda, lalu catat nama filenya pada data iterasi setelahnya juga. Jadi didapat dua nama file yang sama pada beberapa iterasi yang berurtan, data nama tersebut digunakan untuk mencatat path `from` pada point 2c sebelumnya dan didapatkan dua file dengan folder genre di masing-masing poster pada file tersebut.
### 2E
Pada kasus ini, dibuat masing-masing data dari setiap film dari setiap folder genre, data tersebut dimasukkan dalam data.txt disetiap foldernya. Cara saya mendapat data tersebut dengan cara mengambil list genre apa saja yang ada dari data struct yang telah didapat sebelumnya dengan cara menghilangkan duplikat data genre pada list tersebut, dan dibuat struct baru untuk menyimpannya.
```
...
  struct drakor genre_list[101];
    int index_list=flag;
    for(int i=0; i<flag; i++){
        strcpy(genre_list[i].genre, (ptr+i)->genre);
    }
    //remove duplicate genre in struct
    for(int i=0; i<index_list; i++){
        for(int j=i+1; j<index_list; j++){
            if(strcmp(genre_list[i].genre, genre_list[j].genre)==0){
                for(int k=j; k<index_list; k++){
                    strcpy(genre_list[k].genre, genre_list[k+1].genre);
                }
                j--;
                index_list--;
            }
        }
    }
...
```
Selanjutnya, cari data yang bergenre sesuai index array yang berlaku, misal jika pada index pertama ada genre romance, cari semua data yang bergenre romance dan catat data tersebut pada struct temp, lalu urutkan data tersebut dengan bubble sort menggunakan acuan `data.year`.
```
...
    for(int i=0; i<index_list; i++){
        struct drakor temp[101];
        int index_temp=0;
        for(int j=0; j<flag; j++){
            if(strcmp(genre_list[i].genre, (ptr+j)->genre)==0){
                strcpy(temp[index_temp].title, (ptr+j)->title);
                temp[index_temp].year=(ptr+j)->year;
                index_temp++;
            }
        }
        //bubble sort data poster sesuai year
        for(int j=0; j<index_temp-1; j++){
            int min_idx=j;
            for(int k=j+1; k<index_temp; k++){
                if(temp[k].year<temp[min_idx].year){
                    min_idx=k;
                    struct drakor another_temp;
                    strcpy(another_temp.title, temp[min_idx].title);
                    another_temp.year=temp[min_idx].year;
                    strcpy(temp[min_idx].title, temp[j].title);
                    temp[min_idx].year=temp[j].year;
                    strcpy(temp[j].title, another_temp.title);
                    temp[j].year=another_temp.year;
                }
            }
        }
...
```
kemudia tulis semua datanya pada variabel isi dengan `strcpy` dan `strcat`. jangan lupa gunakan pid dan fork lalu `touch` data.txt. Buat juga path untuk menuliskan data tersebut pada data.txt di folder genre-nya. Gunakan fopen dengan kode `a` atau append untuk open for writting dan gunakan fputs untuk menuliskan data yang telah disusun di variabel isi.
```
...
        char text_path[1001];
        strcpy(text_path, "/home/ilhamfz/Sisop/Shift2/drakor/");
        strcat(text_path, genre_list[i].genre);
        strcat(text_path, "/data.txt");

        pid_t func;
        func = fork();
        int status;

        if (func < 0) {
            exit(EXIT_FAILURE);
        }
        if (func == 0){
            char *argv[] = {"touch", text_path, NULL};
            execv("/usr/bin/touch", argv);
        }else {
            while ((wait(&status)) > 0);
        }

        char isi[1001];
        strcpy(isi, "kategori : ");
        strcat(isi, genre_list[i].genre);
        for(int i=0; i<index_temp; i++){
            strcat(isi, "\n\n");
            strcat(isi, "nama : ");
            strcat(isi, temp[i].title);
            strcat(isi, "\n");
            strcat(isi, "rilis : ");
            char hmm[11];
            sprintf(hmm, "%d",(temp[i].year));
            strcat(isi, hmm);
        }
        FILE *fptr=fopen(text_path, "a");
        fputs(isi, fptr);
        fclose(fptr);
    }
    return;
}
...
```
Step trakhir yang harus dilakukan ialah menghapus file png diluar folder genre, disinu saya menggunakan data filename dari struct drakor dan menggunakan syntax shell berupa `find` dan mengkombinasikannya dengan `-delet`.
```
...
        char del_path[1001];
        strcpy(del_path, locpwd);
        strcat(del_path, "/");
        strcat(del_path, (ptr+i)->file_name);
        char *argv[] = {"find",del_path, "-delete", NULL};
        execv("/bin/find", argv);
...
```
Jangan lupa gunakan pid dan fork() pid tersebut.
### 2 Hasil
![Screenshot_from_2022-03-27_01-18-08](/uploads/39218ec09f4fee745f3f09cc2ebc1b7f/Screenshot_from_2022-03-27_01-18-08.png)

![Screenshot_from_2022-03-27_01-18-41](/uploads/2f3a8ea9373f14dd333acbb1398325e3/Screenshot_from_2022-03-27_01-18-41.png)

![Screenshot_from_2022-03-27_01-19-10](/uploads/dabd9434204a5dbfab46f91454506270/Screenshot_from_2022-03-27_01-19-10.png)

![Screenshot_from_2022-03-27_01-19-27](/uploads/d00003ec161bb2b74f3af646d9a68164/Screenshot_from_2022-03-27_01-19-27.png)

![Screenshot_from_2022-03-27_01-19-49](/uploads/84513b6f1d21fce0595524948cad1132/Screenshot_from_2022-03-27_01-19-49.png)

![Screenshot_from_2022-03-27_01-20-13](/uploads/adadd1b0d98e122ffc3370328c9b4e7a/Screenshot_from_2022-03-27_01-20-13.png)

![Screenshot_from_2022-03-27_01-20-33](/uploads/0097bfd242770d79d3373bf287f87628/Screenshot_from_2022-03-27_01-20-33.png)

![Screenshot_from_2022-03-27_01-20-49](/uploads/158ef9c1a5c0207c75d51771b349fb4b/Screenshot_from_2022-03-27_01-20-49.png)

## Nomer 3

Conan adalah seorang detektif terkenal. Suatu hari, Conan menerima beberapa laporan tentang hewan di kebun binatang yang tiba-tiba hilang. Karena jenis-jenis hewan yang hilang banyak, maka perlu melakukan klasifikasi hewan apa saja yang hilang.
a. Untuk mempercepat klasifikasi, Conan diminta membuat program untuk membuat 2 directory di “/home/[USER]/modul2/” dengan nama “darat” lalu 3 detik kemudian membuat directory ke 2 dengan nama “air”. 
b. Kemudian program diminta dapat melakukan extract “animal.zip” di “/home/[USER]/modul2/”.
c. Tidak hanya itu, hasil extract dipisah menjadi hewan darat dan hewan air sesuai dengan nama filenya. Untuk hewan darat dimasukkan ke folder “/home/[USER]/modul2/darat” dan untuk hewan air dimasukkan ke folder “/home/[USER]/modul2/air”. Rentang pembuatan antara folder darat dengan folder air adalah 3 detik dimana folder darat dibuat terlebih dahulu. Untuk hewan yang tidak ada keterangan air atau darat harus dihapus.
d. Setelah berhasil memisahkan hewan berdasarkan hewan darat atau hewan air. Dikarenakan jumlah burung yang ada di kebun binatang terlalu banyak, maka pihak kebun binatang harus merelakannya sehingga conan harus menghapus semua burung yang ada di directory “/home/[USER]/modul2/darat”. Hewan burung ditandai dengan adanya “bird” pada nama file.
e.Conan harus membuat file list.txt di folder “/home/[USER]/modul2/air” dan membuat list nama semua hewan yang ada di directory “/home/[USER]/modul2/air” ke “list.txt” dengan format UID_[UID file permission]_Nama File.[jpg/png] dimana UID adalah user dari file tersebut file permission adalah permission dari file tersebut.
Catatan :
- Tidak boleh memakai system().
- Tidak boleh memakai function C mkdir() ataupun rename().
- Gunakan exec dan fork
- Direktori “.” dan “..” tidak termasuk



### a. Membuat direktori darat, lalu 5 detik kemudian membuat direktori air
```
int main() {
  pid_t child_a, child_b, child_c, child_d;
  int status;
  child_a = fork();
  if (child_a < 0) 
  {
    exit(EXIT_FAILURE); 
  }
 
  if (child_a == 0) 
  {
    char *mkd[] = {"mkdir", "/home/ozoraa/modul2/darat", NULL};
    execv("/bin/mkdir", mkd);
  } 
```
Menginisiasi pid yang akan digunakan dan int status. Jika ```child_a < 0``` maka  gagal membuat proses baru, program akan berhenti. Child membuat directory baru dengan nama darat.
```
else 
  {
    child_b = fork();
    if (child_b < 0) 
    {
      exit(EXIT_FAILURE); 
    }
    if (child_b == 0)
    { 
      while ((wait(&status)) > 0);
      sleep(3);
      char *argv[] = {"mkdir","/home/ozoraa/modul2/air", NULL};
      execv("/bin/mkdir", argv);
    }
    
  ```
 Membuat fork untuk melakukan proses baru yaitu membuat directory dengan nama 'air' yang berjalan setelah 5 detik dari proses sebelumnya yang dibuat dengan menggunakan wait.
 
 
 
 ### b. Meng-ekstrak file animal.zip di direktori “/home/ozoraa/modul2/”

```
 else 
    {
      while ((wait(&status)) > 0);
      child_c = fork();
      if (child_c < 0) 
      {
        exit(EXIT_FAILURE); 
      }
      if(child_c == 0) 
      {
        char *unzip[]={"unzip", "/home/ozoraa/modul2/animal.zip", NULL};
     	  execv("/usr/bin/unzip",unzip);
      }	  
```
Membuat fork untuk melakukan proses baru yaitu meng-ekstrak file animal.zip di direktori “/home/ozoraa/modul2/”.


### c. hasil dari ekstrakan tersebut dipindahkan sesuai dengan pengelompokan,  Untuk hewan darat dimasukkan ke folder “/home/ozoraa/modul2/darat” dan untuk hewan air dimasukkan ke folder “/home/ozoraa/modul2/air”” Untuk yang tidak ada keterangan maka akan langsung di hapus

```
   else 
      {
        while ((wait(&status)) > 0);
        DIR *d;
        struct dirent *dir; 
        d = opendir("animal");
        int masuk = 0;
        if (d)
        {
          while ((dir = readdir(d)) != NULL)
          {
            char s[10] = {'\0'};
            char dir_name[1000] = {'\0'};
            child_d = fork ();
            if (child_d < 0) 
            {
              exit(EXIT_FAILURE); 
            }
            if (child_d == 0)
            {
              struct stat info;
              char namafile[1000];
              sprintf(namafile, "/home/ozoraa/modul2/animal/%s", dir->d_name);
              strcpy(dir_name,dir->d_name);
              for(int i = 0; i < strlen(dir_name); i++)
              {
                if(dir_name[i]=='_'&&dir_name[i+1]=='d' || dir_name[i]=='_'&&dir_name[i+1]=='b')

                {
                  strcpy(s,"darat");
                }
                else if( (dir_name[i]=='_'&&(dir_name[i+1]=='a'||dir_name[i+1]=='i')))
                {
                  strcpy(s,"air");
                }
              }

              if (stat(namafile, &info)) 
              {
                exit (EXIT_FAILURE);
              }
              
              if(strcmp(s,"darat") == 0)
              {
                char* move[] = {"mv", namafile, "/home/ozoraa/modul2/darat/", NULL}; 
                execv("/bin/mv", move);

              }
              if(strcmp(s,"air") == 0)
              {
                 char* move[] = {"mv", namafile, "/home/ozoraa/modul2/air/", NULL};
                 execv("/bin/mv", move);             
              }
              else{
                remove(namafile);
              }
            }
          }
          closedir(d);
        }
      }
```
Untuk mengetahui format isi dari sebuah direktori, kita menggunakan DIR. Lalu membuka dan membaca file dalam folder "animal" dan melakukan proses pemindahan file dari "/home/ozoraa/modul2/animal” ke "/home/ozoraa/modul2/darat” dan "/home/ozoraa/modul2/air” sesuai dengan nama , jika tidak ada keterangan, maka akan dihapus . 

### d.  menghapus semua burung yang ada di directory “/home/ozoraa/modul2/darat”. Hewan burung ditandai dengan adanya “bird”
```
  DIR *d;
  struct dirent *dir; 
  d = opendir("/home/ozoraa/modul2/darat");
  if(d)
  {
   while ((dir = readdir(d)) != NULL)
    {
      char s[10] = {'\0'};  
      char dir_name[1000] = {'\0'};
      char namafile[1000];
      sprintf(namafile, "/home/ozoraa/modul2/darat/%s", dir->d_name);
      strcpy(dir_name,dir->d_name);
      for(int i = 0; i < strlen(namafile); i++)
        {
          if(namafile[i]=='b'&&namafile[i+1]=='i'&&namafile[i+2]=='r'&&namafile[i+3]=='d')
              {
                strcpy(s,"bird");

              }
        }
      if(strcmp(s,"bird")==0)
        {
          remove(namafile);
        }
     }
                 
  }
 closedir(d);
```

### e. membuat file list.txt di folder “/home/[USER]/modul2/air” dan membuat list nama semua hewan yang ada di directory “/home/[USER]/modul2/air” ke “list.txt” dengan format UID_[UID file permission]_Nama File.[jpg/png].

```
DIR *d;
 struct dirent *dir; 
 d = opendir("/home/ozoraa/modul2/air");
 struct stat fileStat;
 char full_path[1000];
 char path[1000]="/home/ozoraa/modul2/air";
 char *filename = "/home/ozoraa/modul2/air/list.txt";
 FILE *fp = fopen(filename, "w");
 if(d)
  {
    while ((dir = readdir(d)) != NULL)
    {
      if(dir->d_type==DT_REG)
      {
        full_path[0]='\0';
        printf ("%s ", dir->d_name);
        strcpy (full_path, path);
        strcat (full_path, "/");
        strcat (full_path, dir->d_name);
        if (!stat(path, &fileStat))
          { fprintf(fp,"conan_");
            fprintf(fp,(S_ISDIR(fileStat.st_mode))  ? "d" : "-");
            fprintf(fp,(fileStat.st_mode & S_IRUSR) ? "r" : "-");
            fprintf(fp,(fileStat.st_mode & S_IWUSR) ? "w" : "-");
            fprintf(fp,(fileStat.st_mode & S_IXUSR) ? "x" : "-");
            fprintf(fp,(fileStat.st_mode & S_IRGRP) ? "r" : "-");
            fprintf(fp,(fileStat.st_mode & S_IWGRP) ? "w" : "-");
            fprintf(fp,(fileStat.st_mode & S_IXGRP) ? "x" : "-");
            fprintf(fp,(fileStat.st_mode & S_IROTH) ? "r" : "-");
            fprintf(fp,(fileStat.st_mode & S_IWOTH) ? "w" : "-");
            fprintf(fp,(fileStat.st_mode & S_IXOTH) ? "x" : "-");
          }
           else
          {
          perror("Error in stat");
          }
          fprintf( fp,"_%s\n",dir->d_name);
      }
    }
  fclose(fp);
  closedir(d);
  }
```
### HASIL
![WhatsApp_Image_2022-03-27_at_17.31.49](/uploads/68ccb464a3f904325a9ea1e6722a3e6a/WhatsApp_Image_2022-03-27_at_17.31.49.jpeg)

![WhatsApp_Image_2022-03-27_at_17.31.49__1_](/uploads/62e9fe4c42d2a9a53f4ec9f9012545d2/WhatsApp_Image_2022-03-27_at_17.31.49__1_.jpeg)

![WhatsApp_Image_2022-03-27_at_17.31.49__2_](/uploads/067be1e3e97a644155fe2b8382bb5404/WhatsApp_Image_2022-03-27_at_17.31.49__2_.jpeg)

![WhatsApp_Image_2022-03-27_at_17.31.50](/uploads/a20aece4519229df73c870cb3c8a9131/WhatsApp_Image_2022-03-27_at_17.31.50.jpeg)









</div>
