#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <dirent.h>
#include <stdio.h>
#include <wait.h>

struct drakor {
    char file_name[1001];
    char title[101];
    int year;
    char genre[101];
};

char *locpwd="/home/ilhamfz/Sisop/Shift2/drakor";
char *loczip="/home/ilhamfz/Sisop/Shift2/drakor.zip";

void create_genfo(struct drakor *ptr, int index, char path[]){
    char temp[101];
    strcpy(temp, path);
    strcat(temp, "/");
    strcat(temp, (ptr+index)->genre);

    pid_t func;
    func = fork();
    int status;

    if (func < 0) {
        exit(EXIT_FAILURE);
    }
    if (func == 0){
        char *argv[] = {"mkdir", "-p", temp, NULL};
        execv("/bin/mkdir", argv);
    } else {
        while ((wait(&status)) > 0);
        return;
    }
}

void copy_movre(struct drakor *ptr, int index, char path[]){
    char from[101];
    strcpy(from, path);
    strcat(from, "/");
    strcat(from, (ptr+index)->file_name);
    char dest[101];
    strcpy(dest, path);
    strcat(dest, "/");
    strcat(dest, (ptr+index)->genre);
    strcat(dest, "/");
    strcat(dest, (ptr+index)->title);
    strcat(dest, ".png");

    pid_t func;
    func = fork();
    int status;

    if (func < 0) {
        exit(EXIT_FAILURE);
    }
    if (func == 0){
        char *argv[] = {"cp", from, dest, NULL};
        execv("/usr/bin/cp", argv);
    }else {
        while ((wait(&status)) > 0);
        return;
    }
}

void check_aja(struct drakor *ptr, int flag){
    for(int i=0; i<flag; i++){
        printf("file name : %s\n", (ptr+i)->file_name);
        printf("title : %s\n", (ptr+i)->title);
        printf("year : %d\n", (ptr+i)->year);
        printf("genre : %s\n", (ptr+i)->genre);
        printf("\n");
    }
    return;
}

void find_genre_list(struct drakor *ptr, int flag){
    struct drakor genre_list[101];
    int index_list=flag;
    for(int i=0; i<flag; i++){
        strcpy(genre_list[i].genre, (ptr+i)->genre);
    }
    //remove duplicate genre in struct
    for(int i=0; i<index_list; i++){
        for(int j=i+1; j<index_list; j++){
            if(strcmp(genre_list[i].genre, genre_list[j].genre)==0){
                for(int k=j; k<index_list; k++){
                    strcpy(genre_list[k].genre, genre_list[k+1].genre);
                }
                j--;
                index_list--;
            }
        }
    }
    //just check genre list
    printf("Ada %d genre, yaitu :", index_list);
    for(int i=0; i<index_list; i++){
        printf("%s\n", genre_list[i].genre);
    }
    //text-it
    for(int i=0; i<index_list; i++){
        struct drakor temp[101];
        int index_temp=0;
        for(int j=0; j<flag; j++){
            if(strcmp(genre_list[i].genre, (ptr+j)->genre)==0){
                strcpy(temp[index_temp].title, (ptr+j)->title);
                temp[index_temp].year=(ptr+j)->year;
                index_temp++;
            }
        }
        //bubble sort data poster sesuai year
        for(int j=0; j<index_temp-1; j++){
            int min_idx=j;
            for(int k=j+1; k<index_temp; k++){
                if(temp[k].year<temp[min_idx].year){
                    min_idx=k;
                    struct drakor another_temp;
                    strcpy(another_temp.title, temp[min_idx].title);
                    another_temp.year=temp[min_idx].year;
                    strcpy(temp[min_idx].title, temp[j].title);
                    temp[min_idx].year=temp[j].year;
                    strcpy(temp[j].title, another_temp.title);
                    temp[j].year=another_temp.year;
                }
            }
        }
        char text_path[1001];
        strcpy(text_path, "/home/ilhamfz/Sisop/Shift2/drakor/");
        strcat(text_path, genre_list[i].genre);
        strcat(text_path, "/data.txt");

        pid_t func;
        func = fork();
        int status;

        if (func < 0) {
            exit(EXIT_FAILURE);
        }
        if (func == 0){
            char *argv[] = {"touch", text_path, NULL};
            execv("/usr/bin/touch", argv);
        }else {
            while ((wait(&status)) > 0);
        }

        char isi[1001];
        strcpy(isi, "kategori : ");
        strcat(isi, genre_list[i].genre);
        for(int i=0; i<index_temp; i++){
            strcat(isi, "\n\n");
            strcat(isi, "nama : ");
            strcat(isi, temp[i].title);
            strcat(isi, "\n");
            strcat(isi, "rilis : ");
            char hmm[11];
            sprintf(hmm, "%d",(temp[i].year));
            strcat(isi, hmm);
        }
        FILE *fptr=fopen(text_path, "a");
        fputs(isi, fptr);
        fclose(fptr);
    }
    return;
}

void delete_outside(struct drakor *ptr, int i){
    pid_t func;
    func = fork();
    int status;

    if (func < 0) {
        exit(EXIT_FAILURE);
    }
    if (func == 0){
        char del_path[1001];
        strcpy(del_path, locpwd);
        strcat(del_path, "/");
        strcat(del_path, (ptr+i)->file_name);
        char *argv[] = {"find",del_path, "-delete", NULL};
        execv("/bin/find", argv);
    }else {
        while ((wait(&status)) > 0);
        return;
    }
}

int main() {
    pid_t child_a;
    int status, flag=0;

    child_a = fork();

    if (child_a < 0) {
        exit(EXIT_FAILURE);
    }
    if (child_a == 0) {
        char *argv[] = {"mkdir", "-p", locpwd, NULL};
        execv("/bin/mkdir", argv);
    } else {
        while ((wait(&status)) > 0);
        pid_t child_ahalf;
        child_ahalf = fork();
        if (child_ahalf < 0) {
            exit(EXIT_FAILURE);
        }
        if (child_ahalf == 0){
            char *argv[] = {"unzip", "-d", locpwd, loczip, NULL};
            execv("/usr/bin/unzip", argv);
        } else{
            while ((wait(&status)) > 0);
            pid_t child_b;
            child_b = fork();
            if (child_b < 0) {
                exit(EXIT_FAILURE);
            }
            if (child_b == 0){
                char *argv[] = {"find", locpwd, "-mindepth", "1", "-maxdepth", "1", "-not", "-name", "*.png", "-exec", "rm", "-rf", "{}", "+", NULL};
                execv("/usr/bin/find", argv);
            } else  {
                while ((wait(&status)) > 0);
                struct drakor data[1001];
                struct dirent *dp;
                DIR *dir = opendir(locpwd);
                while ((dp = readdir(dir)) != NULL){
                    if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0){
                        int another_flag=0;
                        strcpy(data[flag].file_name, dp->d_name);
                        if(strchr(data[flag].file_name, '_')!=0) another_flag++;
                        char *tok = strtok(dp->d_name, "_;.");
                        for(int i=0; tok!=NULL; i++){
                            if(i==0) strcpy(data[flag].title, tok);
                            else if (i==1) data[flag].year=atoi(tok);
                            else if (i==2) strcpy(data[flag].genre, tok);
                            else if (i==3) { //iterasi sampai pada '_' atau '.png'
                                flag++;
                                if(another_flag!=0){
                                    strcpy(data[flag].file_name, data[flag-1].file_name);
                                    strcpy(data[flag].title, tok);
                                    another_flag=0;
                                }
                                i=0;
                            }
                            tok = strtok (NULL, "_;.");
                        }
                    }
                }
                for(int i=0; i<flag; i++) create_genfo(data, i, locpwd);
                for(int i=0; i<flag; i++) copy_movre(data, i, locpwd);
                check_aja(data, flag);
                find_genre_list(data, flag);
                //delete png file outside the genre folder
                for(int i=0; i<flag; i++) delete_outside(data, i);
                char *argv[] = {"find", locpwd, NULL};
                execv("/bin/find", argv);
                closedir(dir);
            }
        }
    }
}
